// Strings
let str = "Hello, world!";
str.length; // Length of string
str.charAt(0); // Character at index
str.concat("!"); // Concatenates strings
str.includes("world"); // Checks if string contains substring
str.indexOf("o"); // First occurrence of 'o'
str.lastIndexOf("o"); // Last occurrence of 'o'
str.replace("Hello", "Hi"); // Replace parts of the string
str.slice(0, 5); // Extracts part of a string
str.split(","); // Splits string into an array of substrings
str.toLowerCase(); // Converts to lowercase
str.toUpperCase(); // Converts to uppercase
str.trim(); // Removes whitespace from both ends of a string

// Arrays
let arr = [1, 2, 3, 4, 5];
arr.length; // Length of array
arr.push(6); // Add item to the end
arr.pop(); // Remove item from the end
arr.shift(); // Remove item from the beginning
arr.unshift(0); // Add item to the beginning
arr.indexOf(3); // First index of item
arr.lastIndexOf(3); // Last index of item
arr.includes(2); // Check if array includes item
arr.slice(0, 2); // Returns selected elements as new array
arr.splice(2, 0, "a", "b"); // Adds/removes items from array
arr.concat([6, 7]); // Merge two or more arrays
arr.join("-"); // Joins all elements of array into a string
arr.reverse(); // Reverses the order of the elements
arr.sort(); // Sorts the elements of an array

// Numbers
let num = 123.456;
Math.round(num); // Rounds to nearest integer
Math.ceil(num); // Rounds up
Math.floor(num); // Rounds down
Math.abs(num); // Absolute value
Math.max(1, 2, 3); // Returns the largest of zero or more numbers
Math.min(1, 2, 3); // Returns the smallest of zero or more numbers
Math.random(); // Generates a random number between 0 and 1
Math.pow(2, 3); // Base 2 raised to the power of 3
Math.sqrt(64); // Square root

// Objects
let obj = { name: "John", age: 30 };
Object.keys(obj); // Array of a given object's own property names
Object.values(obj); // Array of a given object's own enumerable property values
Object.entries(obj); // Array of a given object's own enumerable string-keyed property [key, value] pairs

// Functions for handling JSON
JSON.stringify(obj); // Converts an object into a JSON string
JSON.parse('{"name":"John","age":30}'); // Parses a JSON string, constructing the JavaScript value or object described by the string

// Note: This list is not exhaustive but includes some of the most commonly used methods and properties.
