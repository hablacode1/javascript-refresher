// Variables, Types, and Operators in JavaScript

var name = "Carlos Vasquez"; // functioned scoped or globally scopped
let age = 30; // block scoped
const birthday = "1996-05-23" // block scoped and must be initialized

// TYPES
let message = "Hello, World"; // String
let count = 100 // Number
let isApproved = false; // Boolean
let notDefined; // undefined
let selectColor = null; // Null
let person = { firstName: "John", lastName: "Doe" }

// OPERATORS
// ---------
// Arithmetic Operators
let sum = 10 + 5; // 15
// Comparison Operators 
let isEqual = (10 === 10); // true
// Logical Operators
let isTrue = (5 > 3) && (10 < 15); // true
// Assignment Operators
let x = 15;
x += 5; // x = x + 5 = 15

// OPERATROS EXERCISE
 
// Calculate the area of a rectangle with length 5 and width 7 using arithmetic operators. Check if the area is greater than 50 using a comparison operator. Store the result in a variable and use a logical operator to evaluate if the area is greater than 50 and less than 100.

let number100 = 100;
let number50 = 50;

function calculateArea(length, width) {
  let area = length * width;

  if(area > number50) {
    return "Yes, area is greater than 50"
  } else if (area < 100) {
    return "Yes Area is less than than 100"
  } else {
    return "Please provide an a value for area"
  }

  // why is this unreacheable code?
  // Answer: the below is unreachable becuase it's declared after a 'return' statement within the function 'calculateArea'. Since all code paths in the 'if-else' statements  lead to a "return" before reaching the declaration of 'let number', this variable becomes unreachable

  // let number = 100;

  // console.log(number)
}

console.log(calculateArea(8,7));