let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// Incremenet by 2
for (let i = 0; i <= 10; i += 2) {
  // console.log(i);
}

// Decrement from 10 to 1
for (let i = 10; i > 0; i--) {
  // console.log(i)
}

// Using a variable to set the start, end, or increment. use variables for any of the three parameters to make the loop more dynamic.

let start = 4;
let end = 7;
let step = 2;

for (let i = start; i <= end; i  += step) {
  // console.log(i); // Output: 3, 5, 7
}

// Looping in reverse

// console.log(numbers.length, ' :numbers length')

for (let i = numbers.length; i > 0; i--) {
  // console.log(i);
}

let names = ['carlos', 'juan', 'firulais'];

for (let i = names.length - 1; i >= 0; i--) {
  console.log(names[i]);
}