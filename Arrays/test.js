// Sequential 'for' loop
// Pros: works on every environement
// Pros: you can use 'break' and 'continue' flow control statements

// Cons: Too verbose
// Imperative
// Easy to have off by one errors (sometimes also called a fence post error)
let myStringArray = ['Heo', 'World'];
for (let i = 0; i < myStringArray.length; i++) {
  console.log(myStringArray[i]);
}

// callback function with 2 parameters
myStringArray.forEach(function (item, index) {
  console.log(item, index);
})

// use only one of the arguments
myStringArray.forEach((x) => {
  if (x.length <= 4) {
    return console.log('less than or equal to 3')
  } else {
    return console.log('Greater than 4')
  }
});