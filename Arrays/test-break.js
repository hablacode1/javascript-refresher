let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 16, 17, 18, 19, 20];

for(let i = 0; i < numbers.length; i++) {
  if (numbers[i] > 15) {
    console.log('Breaking the loop. Number greater than 15: ', numbers[i]);
    // will break when it meets criteria, it will not continue after 16
    break;
  }
  if (numbers[i] % 2 !== 0) {
    continue; // Skips the current loop iteration if number is odd
  }

  console.log('Even Number: ', numbers[i]);
}