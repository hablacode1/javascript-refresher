// Prime Number
// Problem: Give a natural number 'n', determine if the number is prime or not
// isPrime(5) = true (1 * 5 or 5 * 1)
// isPrime(4) = false (1 * 4 or 2 * 2 or 4 * 1)

function PrimeNumber(n) {
  if (n < 2 ) {
    return false 
  }

  value = n % 2

  if (value === 0) {
    return `${false} - ${n}`
  } else if (value === 1) {
    return `${true} - ${n}`
  } else {
    return 'broken'
  }
}

console.log(PrimeNumber(1)); // false
console.log(PrimeNumber(2)); // false
console.log(PrimeNumber(4)); // False
console.log(PrimeNumber(13)); // true
console.log(PrimeNumber(3)); // true
console.log(PrimeNumber(5)); // false
console.log(PrimeNumber(6)); // false

function isPrime(n) {

}