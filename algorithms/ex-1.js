function summation (n) {
  let sum = 0;
  for (let i = 1; i <= n; i++) {
    sum += i;
  }
  return sum;
}

// Time complexity -> 0(1) - Constant
function summation1 (n) {
  // the value of n only executed once
  return (n * (n + 1)) / 2;
}

// Time complexity -> 0(n2) - Quadratic
// 3n2 + 5n + 1
for (i = 1; i <= n; i++) {
  for (j = 1; j <= i; j++) {
    // some code
  }
}

// Space Complexity
// O(1) - Constant
// O(1) - Constant
// O(1) - Constant