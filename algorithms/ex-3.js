// Factorial of a number
// Problem: Give an integer 'n', find the factorial of that integer
// In mathematics, the factorial of a non-negative integer 'n', denoted n!, is the product of all positive integers less than or equal to 'n',
// Factorial is 1

function factorial(n) {
  let result = 1;

  // put it to 2 since, 1 has no effect. 2 * 1 = 1;
  for(let i = 1; i <= n; i++) {
    result *= i  
  }
  return result;
}

// Big-O is linear, as the size of the input increases so does the time complexity O(n)
// Big-O = O(n)

console.log(factorial(0))
console.log(factorial(1))
console.log(factorial(3))
console.log(factorial(4))
console.log(factorial(5))
console.log(factorial(6))
console.log(factorial(7))
console.log(factorial(8))
console.log(factorial(9))
console.log(factorial(10))