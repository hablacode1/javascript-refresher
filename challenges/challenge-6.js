// function to print array
function printArray (arr) {
  let ans = '';
  for (let i = 0; i < arr.length; i++) {
    ans += arr[i] + " ";
  }
  console.log(ans);
}

// a function to generate a random permutation of arr
function randomize (arr) {
  // Start from the last element and swap on by on. We don't need to run for the first element that's why i > 0
  for (let i = arr.length - 1; i > 0; i--){ 
    let j = Math.floor(Math.random() * (i + 1));
    // swap arr[i] with element at random index
    [arr[i], arr[j]] = [arr[j], arr[i]];
    
  }
}

let arr = [1, 2, 3, 4, 5, 6, 7, 8]; 
randomize(arr); 
printArray(arr); 