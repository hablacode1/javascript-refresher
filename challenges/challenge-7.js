function reverse (arr) {
  let emptyArr = [];

  if (arr.length === 0) {
    console.log('Data cant be empty, returning original data set');
    return arr
  }
  
  for(let i = arr.length - 1; i >= 0; i--) {
    emptyArr.push(arr[i])
  }
  
  return emptyArr
}

const words = ['spray', 'elite', 'exuberant', 'destruction', 'present'];
const numbers = [10, 20, 30, 40, 50];

console.log('Reversed Numbers: ', reverse(numbers));
console.log('Reversed Names: ', reverse(words));
console.log('Original: ', numbers);