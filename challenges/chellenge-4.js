function sumValue() {
  const numbers = [];
  const initialValue = 0;
  let sum;

  for (let i = 10; i <= 100; i += 10) {
    numbers.push(i);
  }

  console.log(numbers);

  sum = numbers.reduce(
    (accum, currentValue) => accum + currentValue,
    initialValue
  );

  return sum;
}

console.log(sumValue());
