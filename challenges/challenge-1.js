function findMostFrequentElements(arr) {
  let mf = 1;
  let m = 0;
  let item;


  for (let i = 0; i < arr.length; i++) {
    // nested loop to compare the current item with others in the array
    for (let j = i; j < arr.length; j++){
      // check if the current item matches with another item in the array
      if (arr[i] === arr[j]) {
        m++
      }
      // upfate the most frequent item and its frequency if the current items frequency is higher
      if (mf < m) {
        mf = m;
        item = arr[i]
      }
    }

    m = 0;
  }
  return item
}

console.log(findMostFrequentElements(['apple', 'banana', 'apple', 'orange', 'banana', 'banana']));