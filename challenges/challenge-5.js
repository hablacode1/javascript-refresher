function lengthArr(arr, len) {
  let newArray = [];

  if(len > arr.length) {
    console.log('Length cant be higher than array length')
    return arr
  }

  for (let i = 0; i < arr.length && i < len; i++) {
    newArray.push(arr[i]);
  }

  return newArray.sort(() => Math.random() - len);
}

const data = [
  { name: "carlos" },
  { name: "juan" },
  { name: "firulais" },
  { name: "alejandro" },
  { name: "joselyn" },
];

console.log(lengthArr(data, 4));