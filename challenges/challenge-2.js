
// variable to hold the array of numbers
const numbers = [1, 2, 3, 4, 5, 6]

function giveSum(arr) {
  let sum = 0;

  for (let i = 0; i < arr.length; i++) {
    sum += arr[i]
  }

  return sum;
}

console.log(giveSum(numbers))