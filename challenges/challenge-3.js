function tenValue() {
  const arrayOfNumbers = [];
  const sumValue = 0;
  let sumWithInitial;

  for (let i = 10; i <= 50; i += 10) {
    arrayOfNumbers.push(i);
  }

  sumWithInitial = arrayOfNumbers.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    sumValue
  );

  return sumWithInitial;
}

console.log(tenValue());
