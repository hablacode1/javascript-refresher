// Palindrome
function Palindrome(str) {
  str.toLowerCase();
  let str2 = str.split('').reverse().join(''); 
  return str === str2;
 
}

// console.log(Palindrome('apa'))

function reverseArray(arr) {
  let newArr = []
  for (let i = arr.length - 1; i >= 0; i--) {
    newArr.push(arr[i])
  }

  return newArr;
}

const reversedArray1 = reverseArray([1, 2, 3]);

// console.log(reversedArray1)

// multiplyAll
// Accepts two arrays. it first makes sure they're of equal length; if they are then it will continue down and multiply every number in the first array with every number in the second array and return the sum of all these products

function multiplyAll(arr1, arr2) {
  if (arr1.length !== arr2.length) return undefined;

  let total = 0;
  for (let i of arr1) {
    for (let j of arr2) {
      total += i * j;
    }
  }
  
  return total;
}

let result1 = multiplyAll([1, 2], [5, 6]) // 33
console.log(result1);