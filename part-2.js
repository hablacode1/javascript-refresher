// Control Structures: Loops, Conditionals, and Error Handling

// Control structures are fundamental in programming, allowing you to control the flow of execution based on conditions, perform repetitive tasks, and handle errors gracefully.

// for Loop: Iterates a block of code a number of times:

// for(let i = 0; i < 5; i++) {
//   console.log(`Iteration number ${i}`);
// }

// While Loop: repeats a block of code as long as specified condition is true.

// let i = 0;
// while (i < 5) {
//   console.log(`Iteration number ${i}`);
//   i++;
// }

// do...while Loop: Executes a block of code once, before checking if the condition is true, then repeats the loop as long as the condition is true

// let i = 0;
// do {
//   console.log(`Iteratrion number ${i}`);
//   i++;
// } while (i < 5);


// CONDITIONALS: are used to perform different actions based on different conditions

// if...else statements

// let score = 75;
// if (score >= 50) {
//   console.log("You passed the exam");
// } else {
//   console.log("You failed the exam")
// }

// Switch statements

// let grade = "A";
// switch (grade) { 
//   case "A":
//     console.log("Execellent");
//     break;
//   case "B":
//     console.log("Good");
//     break;
//   case "C":
//     console.log("Okay");
//     break;
//   default:
//     console.log("Unknown Grade");
// }

// Error Handling: in JavaScript is done using the `Try...catch` statement.

// try...catch

// try {
//   nonExistentFunction();
// } catch (error) {
//   console.log(`Caught an error: ${error.message}`);
// }

// Loops Exercise: Write a loop that prints the numbers 1 through 10 to the console.

// for(let i = 0; i < 11; i++) {
//   console.log(`Operator: ${i}`)
// }

// Conditionals Exercise: Write a script that checks the value of a variable and logs whether it is positive, negative, or zero.

// let positive = 1;
// let negative = -1;
// let zero = 0;

// if (positive > 0) {
//   console.log(`Number: ${positive} is positive`)
// }

// if (negative < 0) {
//   console.log(`Number: ${negative} is negative`)
// } 

// if (zero === 0) {
//   console.log(`Number: ${zero} is equal to zero`)
// }

// Error Handling Exercise: Write a function that tries to parse a given string to JSON and handles the case where the string is not valid JSON.

// function parseString(value) {
//  try {
//   const valueParse = JSON.parse(value)
//   return valueParse

//  } catch(error) {
//   console.log(`Caught an error: ${error.message}`);
//  }
// }

// console.log(parseString("Hello, World!"))