const numbers = [10, 20, 30, 40, 50];
const words = ['spray', 'elite', 'exuberant', 'destruction', 'present'];

// you have to go through everything no matter what
// problems with async and you cannot break
// use when you want to make an addition or change to every value
numbers.forEach((value, index) => { 
  console.log(value);
  console.log(index)
})

const result = words.filter((word) => word.startsWith('e') || word.endsWith('t') );

console.log(result)