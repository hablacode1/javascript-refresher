function getById(id) {
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log(`Got ${id}`);
      resolve(id);
    }, 1000);
  })
}

// will return everything in order
// (async function() {
//   const ids = [10, 20, 30];
//   for (const id of ids) {
//     await getById(id);
//   }
// })();

// will return everything at once
(async function() {
  const ids = [10, 20, 30];
  ids.forEach(async(id) => {
    await getById(id);
  });
})();
