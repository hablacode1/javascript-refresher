const numbers = [10, 20, 30, 40, 50];

// if you need the index
for (const index in numbers) {
  console.log(index)
  console.log(numbers[index])
}

// es6
// allow you to break when you meet a certain criteria
// if you need the value
for(const value of numbers) {
  if(value > 20) {
    break
  }
  console.log(value);
}