const numbers = [10, 20, 30, 40, 50];
// const first = numbers[0];
// const second = numbers[1];

// destructuing
const [first, second, third, ...rest] = numbers;

// spread
console.log(...rest)