const numbers = [10, 20, 30, 40, 50];
numbers;
const copyOfNumbers = [...numbers];
copyOfNumbers;
copyOfNumbers[0] = 100;
console.log(copyOfNumbers)
// has not mutated the original array
console.log(numbers)

// objects
const people = [{name: "john"}, {name: "carlos"}];
const copyOfPeople = [...people];
copyOfPeople[0].name = "Jack"
console.log(copyOfPeople)
console.log(people)

// use lodash functions