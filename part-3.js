// Functions in JavaScript: are one of the core building blocks in JavaScript, allowing you to encapsulate reusable code. They come in several flavors and have interesting features regarding scope and closures. Let's dive into each of these topics.

// Function Declaration
// A function declaration defines a function with the specified parameters.

// function greet(name) {
//   return `Hello, ${name}`
// }

// console.log(greet('Carlos'));

// Arrow Function
// Introduced in ES6, arrow functions offer a more concise syntax for writing function expressions. They are especially useful for inline functions and callbacks.

// const greet = (name) => `Hello, ${name}`
// console.log(greet('Angelica'));

// Scope
// Scope determines the visibility or accessibility of variables and functions at various parts of your code.

// let globalVar = "I'm global";

// function checkScope() {
//   let functionvar = "I'm local to this function";
//   if (true) {
//     let blockVar = "I'm local to this block";
//     console.log(blockVar); // Accessible here
//   }
//   console.log(functionvar); // Accessible here
//   // console.log(blockVar); // Uncaught ReferenceError: blockVar is not defined
// }

// checkScope();
// console.log(globalVar);

// CLOSURES
// A closure is a feature where an inner function has access to the outer (enclosing) function’s variables—a scope chain.

function makeGreeting() {
  let name = "Alice";
  return function(greeting) {
    return `${greeting}, ${name}!`;
  }
}

let greetAlice = makeGreeting();
console.log(greetAlice("Hello, Alice!"))

function init() {
  let name = "Carlos Vasquez"; // name is a local variable created by init
  function displayName() {
    // displayName() is the inner function, that forms the closure
    console.log (name);
  }
  displayName();
}

console.log(init());



// EXERCISES:
// -------------
// 1.Function Declaration Exercise: Write a function declaration that takes two numbers as arguments and returns their sum.

// 2.Arrow Function Exercise: Convert the above function declaration into an arrow function.

// 3.Scope Exercise: Create a function that shows the difference between global scope, function scope, and block scope using var, let, and const.

// 4.Closures Exercise: Write a function that creates a private counter. It should return an object with methods to increment, decrement, and getValue.